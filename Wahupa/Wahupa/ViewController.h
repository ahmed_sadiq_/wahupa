//
//  ViewController.h
//  Wahupa
//
//  Created by Ahmed Sadiq on 25/8/17.
//  Copyright © 2017 PeakPointTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>

@interface ViewController : UIViewController<UITextFieldDelegate,GIDSignInUIDelegate>

@property (strong, nonatomic) IBOutlet UITextField *emailTxt;
@property (strong, nonatomic) IBOutlet UITextField *pswdTxt;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *myActivityIndicator;

@property (strong, nonatomic) IBOutlet UIView *loaderView;


@property (strong, nonatomic) IBOutlet UIImageView *checkImg;
- (IBAction)checkBtnPressed:(id)sender;

@property(weak, nonatomic) IBOutlet GIDSignInButton *signInButton;

@end

