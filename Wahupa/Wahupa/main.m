//
//  main.m
//  Wahupa
//
//  Created by Ahmed Sadiq on 25/8/17.
//  Copyright © 2017 PeakPointTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
