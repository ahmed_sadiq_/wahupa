//
//  ViewController.m
//  Wahupa
//
//  Created by Ahmed Sadiq on 25/8/17.
//  Copyright © 2017 PeakPointTechnologies. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void) moveToHomeViaGoogleSignUp {
    
    [self performSegueWithIdentifier:@"signInSegue" sender:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moveToHomeViaGoogleSignUp)
                                                 name:@"SignUpFromGoogle"
                                               object:nil];
    
    _myActivityIndicator.hidden = true;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)checkBtnPressed:(id)sender {
    
    UIButton *chckBtnTemp = (UIButton*) sender;
    if(chckBtnTemp.tag ==0 ) {
        chckBtnTemp.tag = 1;
        _checkImg.image = [UIImage imageNamed:@"checked"];
        
    }
    else {
        chckBtnTemp.tag = 0;
        _checkImg.image = [UIImage imageNamed:@"unchecked"];
    }
    
}

#pragma mark -
#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField  {
}

// pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    _myActivityIndicator.hidden = false;

    [_myActivityIndicator startAnimating];
    
    _loaderView.hidden = false;
    //
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    _myActivityIndicator.hidden = true;
    
    [_myActivityIndicator stopAnimating];
    
    _loaderView.hidden = true;
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    _myActivityIndicator.hidden = true;
    
    [_myActivityIndicator stopAnimating];
    
    _loaderView.hidden = true;
}
@end
