//
//  ForgotPswdVC.h
//  Wahupa
//
//  Created by Ahmed Sadiq on 6/9/17.
//  Copyright © 2017 PeakPointTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPswdVC : UIViewController <UITextFieldDelegate>

@end
