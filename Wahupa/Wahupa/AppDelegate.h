//
//  AppDelegate.h
//  Wahupa
//
//  Created by Ahmed Sadiq on 25/8/17.
//  Copyright © 2017 PeakPointTechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

